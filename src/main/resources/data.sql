INSERT INTO users VALUES (1, 'admin', 'admin');
INSERT INTO users VALUES (2, 'nez', 'nez');
INSERT INTO users VALUES (3, 'trm', 'trm');

INSERT INTO departs VALUES (1, 'Tiền đạo');
INSERT INTO departs VALUES (2, 'Tiền vệ');
INSERT INTO departs VALUES (3, 'Hậu vệ');
INSERT INTO departs VALUES (4, 'Thủ Môn');
INSERT INTO departs VALUES (5, 'Dự bị');

INSERT INTO staffs VALUES (1, 'Iker Casillas', 1, '1981-06-15', 'casillas.png', 'qngnhat@gmail.com', '0983212321', 100000, 5);
INSERT INTO staffs VALUES (2, 'Pepe', 0, '1985-05-11', 'pepe.png', 'qngnhat@gmail.com', '0983212321', 120000, 3);
INSERT INTO staffs VALUES (3, 'Dani Carvajal', 1, '1987-01-22', 'carvajal.png', 'qngnhat@gmail.com', '0983212321', 150000, 3);
INSERT INTO staffs VALUES (4, 'Sergio Ramos', 1, '1986-11-10', 'ramos.png', 'qngnhat@gmail.com', '0983212321', 120000, 5);
INSERT INTO staffs VALUES (5, 'Varane', 0, '1992-02-19', 'varane.png', 'qngnhat@gmail.com', '0983212321', 140000, 3);
INSERT INTO staffs VALUES (6, 'Nacho', 0, '1991-01-01', 'nacho.png', 'qngnhat@gmail.com', '0983212321', 160000, 3);
INSERT INTO staffs VALUES (7, 'Cristiano Ronaldo', 1, '1986-06-25', 'ronaldo.png', 'qngnhat@gmail.com', '0983212321', 200000, 1);
INSERT INTO staffs VALUES (8, 'Toni Kroos', 1, '1989-04-05', 'kroos.png', 'qngnhat@gmail.com', '0983212321', 180000, 2);
INSERT INTO staffs VALUES (9, 'Benzema', 1, '1987-12-14', 'benzema.png', 'qngnhat@gmail.com', '0983212321', 170000, 1);
INSERT INTO staffs VALUES (10, 'Luka Modric', 1, '1986-07-13', 'modric.png', 'qngnhat@gmail.com', '0983212321', 110000, 2);
INSERT INTO staffs VALUES (11, 'Gareth Bale', 0, '1991-09-23', '../images/bale.png', 'qngnhat@gmail.com', '0983212321', 180000, 1);
INSERT INTO staffs VALUES (12, 'Marcelo', 1, '1987-06-20', 'marcelo.png', 'qngnhat@gmail.com', '0983212321', 100000, 3);
INSERT INTO staffs VALUES (13, 'Nez', 1, '1999-05-16', 'nez.png', 'qngnhat@gmail.com', '0983212321', 400000, 2);
INSERT INTO staffs VALUES (14, 'Casemiro', 0, '1990-01-15', 'casemiro.png', 'qngnhat@gmail.com', '0983212321', 120000, 2);

INSERT INTO records VALUES (1, 1, 'Tích cực tham gia tấn công', '2021-02-21', 1);
INSERT INTO records VALUES (2, 0, 'Xử lý lỗi nhiều', '2021-02-21', 5);
INSERT INTO records VALUES (3, 0, 'Phạm lỗi nhiều', '2021-02-24', 2);
INSERT INTO records VALUES (4, 1, 'Ghi bàn thắng', '2021-03-12', 7);
INSERT INTO records VALUES (5, 1, 'Phòng ngự tốt', '2021-03-17', 3);
INSERT INTO records VALUES (6, 1, 'Hoàn thành tốt nhiệm vụ', '2021-03-17', 12);
INSERT INTO records VALUES (7, 1, 'Làm chủ tuyến giữa', '2021-03-24', 13);
INSERT INTO records VALUES (8, 0, 'Cá nhân', '2021-02-12', 11);